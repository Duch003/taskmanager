# README #

This is one of the exam-like projects written while attending CodersLab programming school. Task manager is a console application which helps user magaing his/her own tasks. Application is written in Polish.

### How do I get set up? ###

* Prerequisites: .NET Core 2.1 runtime, Visual Studio
* Download ZIP or clone repository,
* Open TaskManager.sln
* Run and enjoy!

### Additional packages used in the project ###

* [Newtonsoft.Json](https://www.newtonsoft.com/json)